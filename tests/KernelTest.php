<?php
declare(strict_types=1);

namespace JasonYHZ\ServiceSdk\Test;

use JasonYHZ\ServiceSdk\Kernel;
use JasonYHZ\ServiceSdk\Oss\OssService;
use JasonYHZ\ServiceSdk\Oss\Vo\STSTokenVo;
use JasonYHZ\ServiceSdk\ServiceConstent;
use JsonException;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use function PHPUnit\Framework\assertInstanceOf;

class KernelTest extends TestCase
{
    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws JsonException
     */
    public function testGetSTSToken(): void
    {
        $_ENV[ServiceConstent::OSSURL] = 'http://127.0.0.1:8080';
        $ossService = new OssService(new Kernel());
        $stsTokenVo = $ossService->aliyun()->getSTSToken();
        assertInstanceOf(STSTokenVo::class, $stsTokenVo);
    }
}
