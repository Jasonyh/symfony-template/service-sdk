<?php
declare(strict_types=1);

namespace JasonYHZ\ServiceSdk\Oss\Vo;

class OssUpdateSuccessVo
{
    public string $Domain;

    public string $FileName;

    public string $FullName;
}