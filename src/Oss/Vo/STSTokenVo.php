<?php
declare(strict_types=1);

namespace JasonYHZ\ServiceSdk\Oss\Vo;

class STSTokenVo
{

    public string $AccessKeySecret;

    public string $Expiration;

    public string $AccessKeyId;

    public string $SecurityToken;

}