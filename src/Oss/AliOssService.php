<?php
declare(strict_types=1);

namespace JasonYHZ\ServiceSdk\Oss;

use JasonYHZ\ServiceSdk\Kernel;
use JasonYHZ\ServiceSdk\Oss\Vo\OssUpdateSuccessVo;
use JasonYHZ\ServiceSdk\Oss\Vo\STSTokenVo;
use JsonException;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class AliOssService
{
    private string $getSTSTokenPath = "/aliyun/getSTSToken";

    private string $proxyUploadPath = "/aliyun/proxyUpload";

    public function __construct(private Kernel $kernel)
    {
    }


    /**
     * 获取临时 STS 授权 Token 用于前端直传场景
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws JsonException
     */
    public function getSTSToken(): STSTokenVo
    {
        $url = $this->buildUrl($this->getSTSTokenPath);
        $json = $this->kernel->request('GET', $url);
        return $this->kernel->getSerializer()
            ->deserialize(
                $json,
                STSTokenVo::class,
                'json',
                [AbstractObjectNormalizer::DISABLE_TYPE_ENFORCEMENT => true]
            );

    }

    /**
     * 服务器代理中转上传
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws JsonException
     */
    public function proxyUploadPath(string $filePath, string $bucket = '')
    {
        $formFields = [
            'bucket' => $bucket,
            'file'   => DataPart::fromPath($filePath)
        ];
        $formData = new FormDataPart($formFields);

        $url = $this->buildUrl($this->proxyUploadPath);
        $json = $this->kernel->request('PUT', $url, [
            'headers' => $formData->getPreparedHeaders()->toArray(),
            'body'    => $formData->bodyToIterable()
        ]);
        return $this->kernel->getSerializer()
            ->deserialize(
                $json,
                OssUpdateSuccessVo::class,
                'json',
                [AbstractObjectNormalizer::DISABLE_TYPE_ENFORCEMENT => true]
            );
    }


    private function buildUrl(string $path): string
    {
        return $this->kernel->getConfig()->getOssUrl() . $path;
    }

}