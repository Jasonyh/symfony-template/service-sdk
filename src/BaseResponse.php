<?php
declare(strict_types=1);

namespace JasonYHZ\ServiceSdk;

final class BaseResponse
{
    public int $code;

    public string $msg;

    public mixed $data;

}