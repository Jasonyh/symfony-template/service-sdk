<?php
declare(strict_types=1);

namespace JasonYHZ\ServiceSdk;

use Doctrine\Common\Annotations\AnnotationReader;
use JasonYHZ\ServiceSdk\Exception\ServiceLogicException;
use JsonException;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class Kernel
{
    private Config $config;

    private HttpClientInterface $httpClient;

    private SerializerInterface $serializer;

    public function __construct(?Config $config = null, ?HttpClientInterface $httpClient = null)
    {
        if ($config === null) {
            $this->config = new Config();
        } else {
            $this->config = $config;
        }
        if ($httpClient === null) {
            $this->httpClient = HttpClient::create();
        } else {
            $this->httpClient = $httpClient;
        }

        $this->serializer = $this->buildSerializer();
    }

    public function getConfig(): Config
    {
        return $this->config;
    }

    public function getHttpClient(): HttpClientInterface
    {
        return $this->httpClient;
    }

    public function getSerializer(): SerializerInterface
    {
        return $this->serializer;
    }

    /**
     * @param string $method
     * @param string $url
     * @param array $option
     * @return string
     * @throws ClientExceptionInterface
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function request(string $method, string $url, array $option = []): string
    {
        $_option = [];
        $response = $this->getHttpClient()->request($method, $url, array_merge_recursive($_option, $option));
        $content = $response->getContent();

        return $this->checkResponse($content);
    }

    /**
     * @throws JsonException
     */
    private function checkResponse(string $content): string
    {
        $json_decode = json_decode($content, true, 512, JSON_THROW_ON_ERROR);
        $code = $json_decode['code'] ?? -1;
        if ($code !== 0) {
            throw new ServiceLogicException('error code: ' . $code . 'error msg: ' . $json_decode['msg'] ?? 'unknow remote error!');
        }
        return json_encode($json_decode['data'], JSON_THROW_ON_ERROR);
    }

    private function buildSerializer(): SerializerInterface
    {
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer($classMetadataFactory)];
        return new Serializer($normalizers, $encoders);
    }

}