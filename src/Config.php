<?php
declare(strict_types=1);

namespace JasonYHZ\ServiceSdk;

use JasonYHZ\ServiceSdk\Exception\ConfigInitializationException;
use Throwable;

final class Config
{
    private string $ossUrl;

    /**
     * @throws ConfigInitializationException
     */
    public function __construct()
    {
        try {
            $this->ossUrl = $_ENV[ServiceConstent::OSSURL];
        } catch (Throwable $throwable) {
            throw new ConfigInitializationException($throwable->getMessage());
        }
    }

    public function getOssUrl(): string
    {
        return $this->ossUrl;
    }

}