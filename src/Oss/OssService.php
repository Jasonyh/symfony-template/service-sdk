<?php
declare(strict_types=1);

namespace JasonYHZ\ServiceSdk\Oss;

use JasonYHZ\ServiceSdk\Kernel;

final class OssService
{
    public function __construct(private Kernel $kernel)
    {
    }

    public function aliyun(): AliOssService
    {
        return new AliOssService($this->kernel);
    }
}