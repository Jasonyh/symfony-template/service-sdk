微服务聚合调用 SDK

```php
$ossService = new OssService(new Kernel());
$stsTokenVo = $ossService->aliyun()->getSTSToken();
$stsTokenVo->AccessKeySecret;
```

如果是在 Symfony 环境中使用，可以使用依赖注入方式来取代 new 操作