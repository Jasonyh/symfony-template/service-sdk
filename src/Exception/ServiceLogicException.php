<?php
declare(strict_types=1);

namespace JasonYHZ\ServiceSdk\Exception;

use LogicException;

class ServiceLogicException extends LogicException
{

}